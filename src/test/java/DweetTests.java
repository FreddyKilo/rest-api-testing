import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

public class DweetTests extends ServiceTest {

    @Test
    public void dweetHelloWorld() {
        Http.get(DweetRoutes.HELLO_WORLD);
    }

    @Test
    public void verifyCreateWithGet() {
        pathParams.put("thingName", "mr-roboto");
        pathParams.put("params", "hello=world");
        response = Dweets.createDweet(pathParams);

        response.body("with.content.hello", equalTo("world"));
    }

    @Test
    public void verifyCreateWithPost() {
        pathParams.put("thingName", "mr-roboto");

        payload.put("hello", "world");
        payload.put("things", 22);
        payload.put("testing", true);

        response = Dweets.createDweet(pathParams, payload);

        String r = response.extract().body().asString();

        response.body("with.content.hello", equalTo("world"));
        response.body("with.content.things", equalTo(22));
        response.body("with.content.testing", equalTo(true));
    }

}
