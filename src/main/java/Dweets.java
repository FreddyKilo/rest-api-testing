import com.jayway.restassured.response.ValidatableResponse;
import org.json.JSONObject;

import java.util.Map;

public class Dweets {

    public static ValidatableResponse createDweet(Map<String, Object> pathParams, JSONObject jsonObject) {
        return Http.post(DweetRoutes.CREATE_WITH_POST, pathParams, jsonObject);
    }

    public static ValidatableResponse createDweet(Map<String, Object> pathParams) {
        return Http.get(DweetRoutes.CREATE_WITH_GET, pathParams);
    }

}
