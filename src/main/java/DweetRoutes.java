public class DweetRoutes {

    private static final String BASE_URL = "http://dweet.io";

    public static final String HELLO_WORLD = BASE_URL + "/dweet/for/mr-roboto?hello=world";
    public static final String CREATE_WITH_GET = BASE_URL + "/dweet/for/{thingName}?{params}";
    public static final String CREATE_WITH_POST = BASE_URL + "/dweet/for/{thingName}";

}
