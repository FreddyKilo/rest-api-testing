import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ValidatableResponse;
import org.json.JSONObject;

import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class Http {
    
    private static final int OK = 200;
    private static final int CREATED = 201;
    private static final int BAD_REQUEST = 400;
    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;

    protected static Response get(String urlPath) {
        return
            given().
            when().
                get(urlPath).
            then().
                statusCode(OK).
                extract().response();
    }

    protected static ValidatableResponse get(String urlPath, Map<String, Object> params) {
        return
            given().
                contentType(ContentType.JSON).
                pathParameters(params).
            when().
                get(urlPath).
            then().
                statusCode(OK);
    }

    protected static ValidatableResponse post(String urlPath, JSONObject json) {
        return
            given().
                body(json.toString()).
            when().
                post(urlPath).
            then().
                statusCode(OK);
    }

    protected static ValidatableResponse post(String urlPath, Map<String, Object> params, JSONObject json) {
        return
            given().
                contentType(ContentType.JSON).
                pathParams(params).
                body(json.toString()).
            when().
                post(urlPath).
            then().
                statusCode(OK);
    }

}
